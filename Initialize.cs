using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.IO;
using SiteUptimeApiTestbed.Services;
using SiteUptimeApiTestbed.Services.Interfaces;
using RestSharp;
using System.Net;

namespace SiteUptimeApiTestbed
{
    public class Initialize
    {
        public static ITestbedService GetTestbedService()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;

            var services = new ServiceCollection();
            ConfigureServices(services);
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var testbedService = serviceProvider.GetService<ITestbedService>();
                return testbedService;
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ITestbedService, TestbedService>();
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            services.AddOptions();
            services.Configure<Settings>(configuration.GetSection("Settings"));
        }        
    }
}