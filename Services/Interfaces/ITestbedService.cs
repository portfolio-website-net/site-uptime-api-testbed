using System.Collections.Generic;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Services.Interfaces
{
    public interface ITestbedService
    {
        Settings GetSettings();

        IDictionary<string, object> GetAccountDetails(
            string apiUrl, 
            string apiKey, 
            string format);

        IDictionary<string, object> GetMonitors(
            string apiUrl, 
            string apiKey, 
            string format);

        IDictionary<string, object> NewMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            Monitor monitor);

        IDictionary<string, object> EditMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            Monitor monitor);

        IDictionary<string, object> DeleteMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            string id);

        IDictionary<string, object> ResetMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            string id);

        IDictionary<string, object> GetAlertContacts(
            string apiUrl, 
            string apiKey, 
            string format);

        IDictionary<string, object> NewAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            AlertContact alertContact);

        IDictionary<string, object> EditAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            AlertContact alertContact);

        IDictionary<string, object> DeleteAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            string id);

        IDictionary<string, object> GetMaintenanceWindows(
            string apiUrl, 
            string apiKey, 
            string format);

        IDictionary<string, object> NewMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            MaintenanceWindow maintenanceWindow);

        IDictionary<string, object> EditMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            MaintenanceWindow maintenanceWindow);

        IDictionary<string, object> DeleteMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            string id);

        IDictionary<string, object> GetPublicStatusPages(
            string apiUrl, 
            string apiKey, 
            string format);

        IDictionary<string, object> NewPublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            PublicStatusPage publicStatusPage);

        IDictionary<string, object> EditPublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            PublicStatusPage publicStatusPage);

        IDictionary<string, object> DeletePublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            string id);            
    }
}
