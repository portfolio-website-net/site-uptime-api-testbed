using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Extensions.Options;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using Newtonsoft.Json;
using RestSharp;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Services
{
    public class TestbedService : ITestbedService
    {
        private readonly Settings _settings;

        public TestbedService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public Settings GetSettings()
        {
            return _settings;
        }

        public IDictionary<string, object> GetAccountDetails(
            string apiUrl, 
            string apiKey, 
            string format)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/getAccountDetails")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> GetMonitors(
            string apiUrl, 
            string apiKey, 
            string format)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/getMonitors")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> NewMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            Monitor monitor)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/newMonitor")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("friendly_name", monitor.FriendlyName)
                .AddParameter("url", monitor.Url)
                .AddParameter("type", monitor.Type)
                .AddParameter("port", monitor.Port)
                .AddParameter("keyword_type", monitor.KeywordType)
                .AddParameter("keyword_value", monitor.KeywordValue)
                .AddParameter("interval", monitor.Interval)
                .AddParameter("alert_contacts", monitor.AlertContacts);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> EditMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            Monitor monitor)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/editMonitor")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", monitor.Id)
                .AddParameter("friendly_name", monitor.FriendlyName)
                .AddParameter("url", monitor.Url)
                .AddParameter("port", monitor.Port)
                .AddParameter("keyword_type", monitor.KeywordType)
                .AddParameter("keyword_value", monitor.KeywordValue)
                .AddParameter("interval", monitor.Interval)
                .AddParameter("alert_contacts", monitor.AlertContacts);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> DeleteMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            string id)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/deleteMonitor")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", id);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> ResetMonitor(
            string apiUrl, 
            string apiKey, 
            string format,
            string id)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/resetMonitor")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", id);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> GetAlertContacts(
            string apiUrl, 
            string apiKey, 
            string format)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/getAlertContacts")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> NewAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            AlertContact alertContact)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/newAlertContact")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("friendly_name", alertContact.FriendlyName)
                .AddParameter("type", alertContact.Type)
                .AddParameter("value", alertContact.Value);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> EditAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            AlertContact alertContact)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/editAlertContact")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", alertContact.Id)
                .AddParameter("friendly_name", alertContact.FriendlyName)
                .AddParameter("value", alertContact.Value);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> DeleteAlertContact(
            string apiUrl, 
            string apiKey, 
            string format,
            string id)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/deleteAlertContact")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", id);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> GetMaintenanceWindows(
            string apiUrl, 
            string apiKey, 
            string format)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/getMWindows")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> NewMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            MaintenanceWindow maintenanceWindow)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/newMWindow")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("friendly_name", maintenanceWindow.FriendlyName)
                .AddParameter("type", maintenanceWindow.Type)
                .AddParameter("value", maintenanceWindow.Value)
                .AddParameter("start_time", maintenanceWindow.StartTime)
                .AddParameter("duration", maintenanceWindow.Duration);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> EditMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            MaintenanceWindow maintenanceWindow)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/editMWindow")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", maintenanceWindow.Id)
                .AddParameter("friendly_name", maintenanceWindow.FriendlyName)
                .AddParameter("value", maintenanceWindow.Value)
                .AddParameter("start_time", maintenanceWindow.StartTime)
                .AddParameter("duration", maintenanceWindow.Duration);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> DeleteMaintenanceWindow(
            string apiUrl, 
            string apiKey, 
            string format,
            string id)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/deleteMWindow")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", id);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> GetPublicStatusPages(
            string apiUrl, 
            string apiKey, 
            string format)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/getPSPs")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> NewPublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            PublicStatusPage publicStatusPage)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/newPSP")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("friendly_name", publicStatusPage.FriendlyName)
                .AddParameter("monitors", publicStatusPage.Monitors)
                .AddParameter("custom_domain", publicStatusPage.CustomDomain)
                .AddParameter("password", publicStatusPage.Password)
                .AddParameter("sort", publicStatusPage.Sort);
                //.AddParameter("hide_url_links", publicStatusPage.HideUrlLinks)
                //.AddParameter("status", publicStatusPage.Status)

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> EditPublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            PublicStatusPage publicStatusPage)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/editPSP")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", publicStatusPage.Id)
                .AddParameter("friendly_name", publicStatusPage.FriendlyName)
                .AddParameter("monitors", publicStatusPage.Monitors)
                .AddParameter("custom_domain", publicStatusPage.CustomDomain)
                .AddParameter("password", publicStatusPage.Password)
                .AddParameter("sort", publicStatusPage.Sort);
                //.AddParameter("hide_url_links", publicStatusPage.HideUrlLinks)
                //.AddParameter("status", publicStatusPage.Status)

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        public IDictionary<string, object> DeletePublicStatusPage(
            string apiUrl, 
            string apiKey, 
            string format,
            string id)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("/v2/deletePSP")
                .AddParameter("api_key", apiKey)
                .AddParameter("format", format)
                .AddParameter("id", id);

            var response = client.Post(request);

            return GetResponse(response, format);
        }

        private IDictionary<string, object> GetResponse(IRestResponse response, string format)
        {
            IDictionary<string, object> result;

            var responseObj = new
            {
                statusCode = response.StatusCode,
                content = response.Content,
                headers = response.Headers,                
                responseUri = response.ResponseUri,
                errorMessage = response.ErrorMessage,
            };

            Console.WriteLine();
            Console.WriteLine(format.ToUpper());
            Console.WriteLine(JsonConvert.SerializeObject(responseObj));

            if (format == ResponseTypes.Json)
            {
                result = (IDictionary<string, object>)JsonConvert.DeserializeObject<ExpandoObject>(responseObj.content);
            }
            else
            {
                var xmlDoc = XDocument.Parse(responseObj.content);
                string xmlToJsonText = JsonConvert.SerializeXNode(xmlDoc);
                result = (IDictionary<string, object>)JsonConvert.DeserializeObject<ExpandoObject>(xmlToJsonText);
            }

            return result;
        }
    }
}
