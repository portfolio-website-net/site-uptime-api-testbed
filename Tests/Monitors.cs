using System;
using System.Collections.Generic;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using NUnit.Framework;
using System.Linq;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Tests
{
    public class Monitors
    {
        private ITestbedService _testbedService;

        [SetUp]
        public void Setup()
        {
            _testbedService = Initialize.GetTestbedService();
        }

        [Test]
        public void RunMonitorModificationTests()
        {
            var monitorList = new List<Monitor>();            

            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");

                // JSON Values - New
                var guid = Guid.NewGuid().ToString();
                var monitor = new Monitor
                {
                    FriendlyName = "TestbedMonitor" + guid,
                    Url = "http://example.com?TestbedMonitor" + guid,
                    Type = "1",
                    Port = string.Empty,
                    KeywordType = string.Empty,
                    KeywordValue = string.Empty,
                    Interval = "300",
                    AlertContacts = string.Empty
                };

                monitorList.Add(monitor);

                var result = _testbedService.NewMonitor(apiUrl, apiKey, ResponseTypes.Json, monitor);
                ValidateMonitorJson(apiUrl, apiKey, result, monitor);

                // JSON Values - Edit
                monitor.FriendlyName = "TestbedMonitor" + guid + "-edit";
                monitor.Url = "http://example.com?TestbedMonitor" + guid + "-edit";
                monitor.Type = "1";
                monitor.Port = string.Empty;
                monitor.KeywordType = string.Empty;
                monitor.KeywordValue = string.Empty;
                monitor.Interval = "300";
                monitor.AlertContacts = string.Empty;
            
                // JSON - Edit
                result = _testbedService.EditMonitor(apiUrl, apiKey, ResponseTypes.Json, monitor);                           
                ValidateMonitorJson(apiUrl, apiKey, result, monitor, isEdit: true);        

                // JSON - Reset
                result = _testbedService.ResetMonitor(apiUrl, apiKey, ResponseTypes.Json, monitor.Id);                           
                ValidateMonitorJson(apiUrl, apiKey, result, monitor, isEdit: true);         

                // JSON - Delete
                result = _testbedService.DeleteMonitor(apiUrl, apiKey, ResponseTypes.Json, monitor.Id);    
                ValidateMonitorJson(apiUrl, apiKey, result, monitor, isEdit: false, isDeleted: true);

                // -----------------------------------------

                // XML Values - New
                guid = Guid.NewGuid().ToString();
                
                monitor.FriendlyName = "TestbedMonitor" + guid;
                monitor.Url = "http://example.com?TestbedMonitor" + guid;

                // XML - New
                result = _testbedService.NewMonitor(apiUrl, apiKey, ResponseTypes.Xml, monitor);                
                ValidateMonitorXml(apiUrl, apiKey, result, monitor);

                // XML Values - Edit
                monitor.FriendlyName = "TestbedMonitor" + guid + "-edit";
                monitor.Url = "http://example.com?TestbedMonitor" + guid + "-edit";

                // XML - Edit
                result = _testbedService.EditMonitor(apiUrl, apiKey, ResponseTypes.Xml, monitor);                
                ValidateMonitorXml(apiUrl, apiKey, result, monitor, isEdit: true);

                // XML - Reset
                result = _testbedService.ResetMonitor(apiUrl, apiKey, ResponseTypes.Xml, monitor.Id);                
                ValidateMonitorXml(apiUrl, apiKey, result, monitor, isEdit: true);

                // XML Validation - Delete
                result = _testbedService.DeleteMonitor(apiUrl, apiKey, ResponseTypes.Xml, monitor.Id);                
                ValidateMonitorXml(apiUrl, apiKey, result, monitor, isEdit: false, isDeleted: true);
            }
        }     

        private void ValidateMonitorJson(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            Monitor monitor, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var jsonMonitor = (IDictionary<string, object>)result["monitor"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            if (!isEdit && !isDeleted)
            {
                Assert.That(jsonMonitor["status"].ToString(), Is.EqualTo("1"));
            }
            
            var id = int.Parse(jsonMonitor["id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(monitor.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            monitor.Id = id.ToString();

            // JSON Validation
            Console.WriteLine();
            Console.WriteLine($"JSON Validation - GetMonitors");

            result = _testbedService.GetMonitors(apiUrl, apiKey, ResponseTypes.Json);                
            var jsonMonitors = (List<object>)result["monitors"];
            var jsonPagination = (IDictionary<string, object>)result["pagination"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            ProcessRunMonitorEditsGetMonitorsAssertions(
                jsonPagination, 
                jsonMonitors, 
                ResponseTypes.Json,
                monitor,
                isDeleted);
        }

        private void ValidateMonitorXml(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            Monitor monitor, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var xmlMonitor = (IDictionary<string, object>)((IDictionary<string, object>)result["monitor"]);
                
            if (!isEdit && !isDeleted)
            {
                Assert.That(xmlMonitor["@status"].ToString(), Is.EqualTo("1"));
            }
            
            var id = int.Parse(xmlMonitor["@id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(monitor.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            monitor.Id = id.ToString();              

            // XML Validation
            Console.WriteLine();
            Console.WriteLine($"XML Validation - GetMonitors");

            result = _testbedService.GetMonitors(apiUrl, apiKey, ResponseTypes.Xml);                
            var xmlMonitorsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["monitors"]);
            var xmlPagination = (IDictionary<string, object>)xmlMonitorsOuter["pagination"];
            var xmlMonitors = (List<object>)xmlMonitorsOuter["monitor"];

            ProcessRunMonitorEditsGetMonitorsAssertions(
                xmlPagination,
                xmlMonitors, 
                ResponseTypes.Xml,
                monitor,
                isDeleted);
        }

        private void ProcessRunMonitorEditsGetMonitorsAssertions(
            IDictionary<string, object> pagination,
            List<object> monitors,
            string format,
            Monitor monitorObj,
            bool isDeleted = false)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination["offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination["limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination["total"].ToString()), Is.GreaterThanOrEqualTo(0));

            var foundMonitor = false;
            foreach (var monitor in monitors.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(monitor[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                
                if (monitorObj.Id == monitor[prependChar + "id"].ToString())
                {
                    foundMonitor = true;
                    Assert.That(monitor[prependChar + "friendly_name"].ToString(), Is.EqualTo(monitorObj.FriendlyName));                    
                    Assert.That(monitor[prependChar + "url"].ToString(), Is.EqualTo(monitorObj.Url));
                    Assert.That(monitor[prependChar + "port"].ToString(), Is.EqualTo(monitorObj.Port));
                    Assert.That(monitor[prependChar + "keyword_type"].ToString(), Is.EqualTo("0"));
                    Assert.That(monitor[prependChar + "keyword_value"].ToString(), Is.EqualTo(monitorObj.KeywordValue));
                    Assert.That(monitor[prependChar + "interval"].ToString(), Is.EqualTo(monitorObj.Interval)); 
                    break;                   
                }
            }

            if (!isDeleted)
            {
                Assert.IsTrue(foundMonitor);
            }
            else
            {
                Assert.IsFalse(foundMonitor);
            }
        }

        [Test]
        public void GetMonitors()
        {
            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");
            
                // JSON
                var result = _testbedService.GetMonitors(apiUrl, apiKey, ResponseTypes.Json);                
                var jsonMonitors = (List<object>)result["monitors"];
                var jsonPagination = (IDictionary<string, object>)result["pagination"];

                Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
                ProcessGetMonitorsAssertions(jsonPagination, jsonMonitors, ResponseTypes.Json);

                // XML
                result = _testbedService.GetMonitors(apiUrl, apiKey, ResponseTypes.Xml);                            
                var xmlMonitorsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["monitors"]);
                var xmlPagination = (IDictionary<string, object>)xmlMonitorsOuter["pagination"];
                var xmlMonitors = (List<object>)xmlMonitorsOuter["monitor"];

                Assert.That(xmlMonitorsOuter["@stat"].ToString(), Is.EqualTo("ok"));                
                ProcessGetMonitorsAssertions(xmlPagination, xmlMonitors, ResponseTypes.Xml);
            }
        }

        private void ProcessGetMonitorsAssertions(
            IDictionary<string, object> pagination,
            List<object> monitors,
            string format)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination["offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination["limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination["total"].ToString()), Is.GreaterThanOrEqualTo(1));

            foreach (var monitor in monitors.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(monitor[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
            }
        }
    }    
}