using System;
using System.Collections.Generic;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using NUnit.Framework;
using System.Linq;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Tests
{
    public class PublicStatusPages
    {
        private ITestbedService _testbedService;

        [SetUp]
        public void Setup()
        {
            _testbedService = Initialize.GetTestbedService();
        }

        [Test]
        public void RunPublicStatusPageModificationTests()
        {
            var pspList = new List<PublicStatusPage>();            

            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");

                // JSON Values - New
                var guid = Guid.NewGuid().ToString();
                var publicStatusPage = new PublicStatusPage
                {
                    FriendlyName = "TestbedPublicStatusPage" + guid,
                    Monitors = "0",
                    CustomDomain = "test." + guid + ".com",
                    Password = guid.Substring(0, 10) + "-password",
                    Sort = "1",
                    HideUrlLinks = "0",
                    Status = "1"
                };

                pspList.Add(publicStatusPage);
            
                // JSON - New
                var result = _testbedService.NewPublicStatusPage(apiUrl, apiKey, ResponseTypes.Json, publicStatusPage);                
                ValidatePublicStatusPageJson(apiUrl, apiKey, result, publicStatusPage);

                // JSON Values - Edit
                publicStatusPage.FriendlyName = "TestbedPublicStatusPage" + guid + "-edit";
                publicStatusPage.Monitors = "0";
                publicStatusPage.CustomDomain = "test." + guid + "-edit" + ".com";
                publicStatusPage.Password = guid.Substring(0, 10) + "-password" + "-edit";
                publicStatusPage.Sort = "1";
                publicStatusPage.HideUrlLinks = "0";
                publicStatusPage.Status = "1";
            
                // JSON - Edit
                result = _testbedService.EditPublicStatusPage(apiUrl, apiKey, ResponseTypes.Json, publicStatusPage);                
                ValidatePublicStatusPageJson(apiUrl, apiKey, result, publicStatusPage, isEdit: true);

                // JSON Validation - Delete            
                result = _testbedService.DeletePublicStatusPage(apiUrl, apiKey, ResponseTypes.Json, publicStatusPage.Id);                
                ValidatePublicStatusPageJson(apiUrl, apiKey, result, publicStatusPage, isEdit: false, isDeleted: true);

                // -----------------------------------------

                // XML Values - New
                guid = Guid.NewGuid().ToString();
                
                publicStatusPage.FriendlyName = "TestbedPublicStatusPage" + guid;
                publicStatusPage.CustomDomain = "http://test." + guid + ".com";
                publicStatusPage.Password = guid.Substring(0, 10) + "-password";

                // XML - New
                result = _testbedService.NewPublicStatusPage(apiUrl, apiKey, ResponseTypes.Xml, publicStatusPage);                
                ValidatePublicStatusPageXml(apiUrl, apiKey, result, publicStatusPage);

                // XML Values - Edit
                publicStatusPage.FriendlyName = "TestbedPublicStatusPage" + guid + "-edit";
                publicStatusPage.Monitors = "0";
                publicStatusPage.CustomDomain = "test." + guid + "-edit" + ".com";
                publicStatusPage.Password = guid.Substring(0, 10) + "-password" + "-edit";
                publicStatusPage.Sort = "1";
                publicStatusPage.HideUrlLinks = "0";
                publicStatusPage.Status = "1";

                // XML - Edit
                result = _testbedService.EditPublicStatusPage(apiUrl, apiKey, ResponseTypes.Xml, publicStatusPage);                
                ValidatePublicStatusPageXml(apiUrl, apiKey, result, publicStatusPage, isEdit: true);

                // XML Validation - Delete               
                result = _testbedService.DeletePublicStatusPage(apiUrl, apiKey, ResponseTypes.Xml, publicStatusPage.Id);                
                ValidatePublicStatusPageXml(apiUrl, apiKey, result, publicStatusPage, isEdit: false, isDeleted: true);
            }
        }

        private void ValidatePublicStatusPageJson(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            PublicStatusPage publicStatusPage, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var jsonPsp = (IDictionary<string, object>)result["psp"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            
            var id = int.Parse(jsonPsp["id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(publicStatusPage.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            publicStatusPage.Id = id.ToString();

            // JSON Validation
            Console.WriteLine();
            Console.WriteLine($"JSON Validation - GetPublicStatusPages");

            result = _testbedService.GetPublicStatusPages(apiUrl, apiKey, ResponseTypes.Json);                
            var jsonPsps = (List<object>)result["psps"];
            var jsonPagination = (IDictionary<string, object>)result["pagination"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            ProcessRunPublicStatusPageEditsGetPSPsAssertions(
                jsonPagination, 
                jsonPsps, 
                ResponseTypes.Json,
                publicStatusPage,
                isDeleted);
        }

        private void ValidatePublicStatusPageXml(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            PublicStatusPage publicStatusPage, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var xmlPsp = (IDictionary<string, object>)((IDictionary<string, object>)result["psp"]);
                            
            var id = int.Parse(xmlPsp["@id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(publicStatusPage.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            publicStatusPage.Id = id.ToString();              

            // XML Validation
            Console.WriteLine();
            Console.WriteLine($"XML Validation - GetPublicStatusPages");

            result = _testbedService.GetPublicStatusPages(apiUrl, apiKey, ResponseTypes.Xml);                
            var xmlPspsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["psps"]);
            var xmlPagination = (IDictionary<string, object>)xmlPspsOuter["pagination"];
            var xmlPsps = (List<object>)xmlPspsOuter["psp"];

            ProcessRunPublicStatusPageEditsGetPSPsAssertions(
                xmlPagination,
                xmlPsps, 
                ResponseTypes.Xml,
                publicStatusPage,
                isDeleted);
        }

        private void ProcessRunPublicStatusPageEditsGetPSPsAssertions(
            IDictionary<string, object> pagination,
            List<object> psps,
            string format,
            PublicStatusPage publicStatusPageObj,
            bool isDeleted = false)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(0));

            var foundPsp = false;
            foreach (var psp in psps.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(psp[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                
                if (publicStatusPageObj.Id == psp[prependChar + "id"].ToString())
                {
                    foundPsp = true;
                    Assert.That(psp[prependChar + "friendly_name"].ToString(), Is.EqualTo(publicStatusPageObj.FriendlyName));                    
                    Assert.That(psp[prependChar + "monitors"].ToString(), Is.EqualTo("0"));
                    Assert.That(psp[prependChar + "custom_url"].ToString().Replace("https://", string.Empty), Is.EqualTo(publicStatusPageObj.CustomDomain));
                    Assert.That(psp[prependChar + "sort"].ToString(), Is.EqualTo(publicStatusPageObj.Sort));
                    Assert.That(psp[prependChar + "status"].ToString(), Is.EqualTo(publicStatusPageObj.Status));
                    break;
                }
            }

            if (!isDeleted)
            {
                Assert.IsTrue(foundPsp);
            }
            else
            {
                Assert.IsFalse(foundPsp);
            }
        }

        [Test]
        public void GetPublicStatusPages()
        {
            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");
            
                // JSON
                var result = _testbedService.GetPublicStatusPages(apiUrl, apiKey, ResponseTypes.Json);                
                var jsonPublicStatusPages = (List<object>)result["psps"];
                var jsonPagination = (IDictionary<string, object>)result["pagination"];

                Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
                ProcessGetPublicStatusPagesAssertions(jsonPagination, jsonPublicStatusPages, ResponseTypes.Json);

                // XML
                result = _testbedService.GetPublicStatusPages(apiUrl, apiKey, ResponseTypes.Xml);                            
                var xmlPublicStatusPagesOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["psps"]);
                var xmlPagination = (IDictionary<string, object>)xmlPublicStatusPagesOuter["pagination"];
                var xmlPublicStatusPages = (List<object>)xmlPublicStatusPagesOuter["psp"];
            
                ProcessGetPublicStatusPagesAssertions(xmlPagination, xmlPublicStatusPages, ResponseTypes.Xml);
            }
        }

        private void ProcessGetPublicStatusPagesAssertions(
            IDictionary<string, object> pagination,
            List<object> publicStatusPages,
            string format)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(1));

            foreach (var publicStatusPage in publicStatusPages.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(publicStatusPage[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                Assert.That(publicStatusPage[prependChar + "standard_url"].ToString(), Is.Not.Null.Or.Empty);
            }
        }
    }
}