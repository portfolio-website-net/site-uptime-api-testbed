using System;
using System.Collections.Generic;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using NUnit.Framework;
using System.Linq;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Tests
{
    public class AlertContacts
    {
        private ITestbedService _testbedService;

        [SetUp]
        public void Setup()
        {
            _testbedService = Initialize.GetTestbedService();
        }

        [Test]
        public void RunAlertContactModificationTests()
        {
            var alertContactList = new List<AlertContact>();            

            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");

                // JSON Values - New
                var guid = Guid.NewGuid().ToString();
                var alertContact = new AlertContact
                {
                    FriendlyName = "TestbedAlertContact" + guid,
                    Type = "2",
                    Value = "test" + guid + "@test.com"
                };

                alertContactList.Add(alertContact);
            
                // JSON - New
                var result = _testbedService.NewAlertContact(apiUrl, apiKey, ResponseTypes.Json, alertContact);                
                ValidateAlertContactJson(apiUrl, apiKey, result, alertContact);

                // JSON Values - Edit
                alertContact.FriendlyName = "TestbedAlertContact" + guid + "-edit";
            
                // JSON - Edit
                result = _testbedService.EditAlertContact(apiUrl, apiKey, ResponseTypes.Json, alertContact);                
                ValidateAlertContactJson(apiUrl, apiKey, result, alertContact, isEdit: true);

                // JSON Validation - Delete               
                result = _testbedService.DeleteAlertContact(apiUrl, apiKey, ResponseTypes.Json, alertContact.Id);                
                ValidateAlertContactJson(apiUrl, apiKey, result, alertContact, isEdit: false, isDeleted: true);

                // -----------------------------------------

                // XML Values - New
                guid = Guid.NewGuid().ToString();
                
                alertContact.FriendlyName = "TestbedAlertContact" + guid;
                alertContact.Type = "2";
                alertContact.Value = "test" + guid + "@test.com";

                // XML - New
                result = _testbedService.NewAlertContact(apiUrl, apiKey, ResponseTypes.Xml, alertContact);                
                ValidateAlertContactXml(apiUrl, apiKey, result, alertContact);

                // XML Values - Edit
                alertContact.FriendlyName = "TestbedAlertContact" + guid + "-edit";

                // XML - Edit
                result = _testbedService.EditAlertContact(apiUrl, apiKey, ResponseTypes.Xml, alertContact);                
                ValidateAlertContactXml(apiUrl, apiKey, result, alertContact, isEdit: true);

                // XML Validation - Delete             
                result = _testbedService.DeleteAlertContact(apiUrl, apiKey, ResponseTypes.Xml, alertContact.Id);                
                ValidateAlertContactXml(apiUrl, apiKey, result, alertContact, isEdit: false, isDeleted: true);
            }
        }

        private void ValidateAlertContactJson(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            AlertContact alertContact, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var jsonAlertContact = isEdit || isDeleted ? 
                (IDictionary<string, object>)result["alert_contact"] :
                (IDictionary<string, object>)result["alertcontact"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            
            var id = int.Parse(jsonAlertContact["id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(alertContact.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            alertContact.Id = id.ToString();

            // JSON Validation
            Console.WriteLine();
            Console.WriteLine($"JSON Validation - GetAlertContacts");

            result = _testbedService.GetAlertContacts(apiUrl, apiKey, ResponseTypes.Json);                
            var jsonAlertContacts = (List<object>)result["alert_contacts"];
            var jsonPagination = (IDictionary<string, object>)result;

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            ProcessRunAlertContactEditsGetAlertContactsAssertions(
                jsonPagination, 
                jsonAlertContacts, 
                ResponseTypes.Json,
                alertContact,
                isDeleted);
        }

        private void ValidateAlertContactXml(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            AlertContact alertContact, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var xmlAlertContact = isEdit || isDeleted ? 
                (IDictionary<string, object>)((IDictionary<string, object>)result["alert_contact"]) :
                (IDictionary<string, object>)((IDictionary<string, object>)result["alertcontact"]);
                            
            var id = int.Parse(xmlAlertContact["@id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(alertContact.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            alertContact.Id = id.ToString();              

            // XML Validation
            Console.WriteLine();
            Console.WriteLine($"XML Validation - GetAlertContacts");

            result = _testbedService.GetAlertContacts(apiUrl, apiKey, ResponseTypes.Xml);                
            var xmlAlertContactsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["alert_contacts"]);
            var xmlPagination = (IDictionary<string, object>)xmlAlertContactsOuter;
            var xmlAlertContacts = (List<object>)xmlAlertContactsOuter["alert_contact"];

            ProcessRunAlertContactEditsGetAlertContactsAssertions(
                xmlPagination,
                xmlAlertContacts, 
                ResponseTypes.Xml,
                alertContact,
                isDeleted);
        }

        private void ProcessRunAlertContactEditsGetAlertContactsAssertions(
            IDictionary<string, object> pagination,
            List<object> alertContacts,
            string format,
            AlertContact alertContactObj,
            bool isDeleted = false)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(0));

            var foundAlertContact = false;
            foreach (var alertContact in alertContacts.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(alertContact[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                
                if (alertContactObj.Id == alertContact[prependChar + "id"].ToString())
                {
                    foundAlertContact = true;
                    Assert.That(alertContact[prependChar + "friendly_name"].ToString(), Is.EqualTo(alertContactObj.FriendlyName));
                    Assert.That(alertContact[prependChar + "type"].ToString(), Is.EqualTo(alertContactObj.Type));
                    Assert.That(alertContact[prependChar + "value"].ToString(), Is.EqualTo(alertContactObj.Value));
                    break;
                }
            }

            if (!isDeleted)
            {
                Assert.IsTrue(foundAlertContact);
            }
            else
            {
                Assert.IsFalse(foundAlertContact);
            }
        }

        [Test]
        public void GetAlertContacts()
        {
            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");
            
                // JSON
                var result = _testbedService.GetAlertContacts(apiUrl, apiKey, ResponseTypes.Json);                
                var jsonAlertContacts = (List<object>)result["alert_contacts"];
                var jsonPagination = (IDictionary<string, object>)result;

                Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
                ProcessGetAlertContactsAssertions(jsonPagination, jsonAlertContacts, ResponseTypes.Json);

                // XML
                result = _testbedService.GetAlertContacts(apiUrl, apiKey, ResponseTypes.Xml);                            
                var xmlAlertContactsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["alert_contacts"]);
                var xmlPagination = (IDictionary<string, object>)xmlAlertContactsOuter;
                var xmlAlertContacts = (List<object>)xmlAlertContactsOuter["alert_contact"];
            
                ProcessGetAlertContactsAssertions(xmlPagination, xmlAlertContacts, ResponseTypes.Xml);
            }
        }

        private void ProcessGetAlertContactsAssertions(
            IDictionary<string, object> pagination,
            List<object> alertContacts,
            string format)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(1));

            foreach (var alertContact in alertContacts.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(alertContact[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                Assert.That(alertContact[prependChar + "value"].ToString(), Is.Not.Null.Or.Empty);
            }
        }
    }
}