using System;
using System.Collections.Generic;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using NUnit.Framework;

namespace SiteUptimeApiTestbed.Tests
{
    public class AccountDetails
    {
        private ITestbedService _testbedService;

        [SetUp]
        public void Setup()
        {
            _testbedService = Initialize.GetTestbedService();
        }

        [Test]
        public void GetAccountDetails()
        {
            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");
            
                // JSON
                var result = _testbedService.GetAccountDetails(apiUrl, apiKey, ResponseTypes.Json);
                var jsonAccount = (IDictionary<string, object>)result["account"];

                Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
                ProcessGetAccountDetailsAssertions(jsonAccount, ResponseTypes.Json);

                // XML
                result = _testbedService.GetAccountDetails(apiUrl, apiKey, ResponseTypes.Xml);            
                var xmlAccount = (IDictionary<string, object>)result["account"];
                
                ProcessGetAccountDetailsAssertions(xmlAccount, ResponseTypes.Xml);                
            }
        }

        private void ProcessGetAccountDetailsAssertions(
            IDictionary<string, object> account,
            string format)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(account[prependChar + "email"].ToString(), Does.Contain("@").IgnoreCase);
            Assert.That(int.Parse(account[prependChar + "monitor_limit"].ToString()), Is.GreaterThanOrEqualTo(50));
            Assert.That(int.Parse(account[prependChar + "monitor_interval"].ToString()), Is.GreaterThanOrEqualTo(1));
            Assert.That(int.Parse(account[prependChar + "up_monitors"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(account[prependChar + "down_monitors"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(account[prependChar + "paused_monitors"].ToString()), Is.GreaterThanOrEqualTo(0));
        }
    }
}