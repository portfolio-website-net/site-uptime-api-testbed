using System;
using System.Collections.Generic;
using SiteUptimeApiTestbed.Services.Interfaces;
using SiteUptimeApiTestbed.Helpers.Types;
using NUnit.Framework;
using System.Linq;
using SiteUptimeApiTestbed.Objects;

namespace SiteUptimeApiTestbed.Tests
{
    public class MaintenanceWindows
    {
        private ITestbedService _testbedService;

        [SetUp]
        public void Setup()
        {
            _testbedService = Initialize.GetTestbedService();
        }

        [Test, Ignore("Skip temporarily")]
        public void RunMaintenanceWindowModificationTests()
        {
            var maintenanceWindowList = new List<MaintenanceWindow>();            

            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");

                // JSON Values - New
                var guid = Guid.NewGuid().ToString();
                var maintenanceWindow = new MaintenanceWindow
                {
                    FriendlyName = "TestbedMaintenanceWindow" + guid,
                    Type = "1",
                    Value = "0",
                    StartTime = "1641038401",
                    Duration = "30"
                };

                maintenanceWindowList.Add(maintenanceWindow);
            
                // JSON - New
                var result = _testbedService.NewMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Json, maintenanceWindow);                
                ValidateMaintenanceWindowJson(apiUrl, apiKey, result, maintenanceWindow);

                // JSON Values - Edit
                maintenanceWindow.FriendlyName = "TestbedMaintenanceWindow" + guid + "-edit";
                maintenanceWindow.Value = "0";
                maintenanceWindow.StartTime = "1641038402";
                maintenanceWindow.Duration = "31";
            
                // JSON - Edit
                result = _testbedService.EditMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Json, maintenanceWindow);                
                ValidateMaintenanceWindowJson(apiUrl, apiKey, result, maintenanceWindow, isEdit: true);

                // JSON Validation - Delete               
                result = _testbedService.DeleteMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Json, maintenanceWindow.Id);                
                ValidateMaintenanceWindowJson(apiUrl, apiKey, result, maintenanceWindow, isEdit: false, isDeleted: true);

                // -----------------------------------------

                // XML Values - New
                guid = Guid.NewGuid().ToString();
                
                maintenanceWindow.FriendlyName = "TestbedMaintenanceWindow" + guid;
                maintenanceWindow.Type = "1";
                maintenanceWindow.Value = "0";
                maintenanceWindow.StartTime = "1641038401";
                maintenanceWindow.Duration = "30";

                // XML - New
                result = _testbedService.NewMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Xml, maintenanceWindow);                
                ValidateMaintenanceWindowXml(apiUrl, apiKey, result, maintenanceWindow);

                // XML Values - Edit
                maintenanceWindow.FriendlyName = "TestbedMaintenanceWindow" + guid + "-edit";
                maintenanceWindow.Value = "0";
                maintenanceWindow.StartTime = "1641038402";
                maintenanceWindow.Duration = "31";

                // XML - Edit
                result = _testbedService.EditMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Xml, maintenanceWindow);                
                ValidateMaintenanceWindowXml(apiUrl, apiKey, result, maintenanceWindow, isEdit: true);

                // XML Validation - Delete             
                result = _testbedService.DeleteMaintenanceWindow(apiUrl, apiKey, ResponseTypes.Xml, maintenanceWindow.Id);                
                ValidateMaintenanceWindowXml(apiUrl, apiKey, result, maintenanceWindow, isEdit: false, isDeleted: true);
            }
        }

        private void ValidateMaintenanceWindowJson(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            MaintenanceWindow maintenanceWindow, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var jsonMaintenanceWindow = (IDictionary<string, object>)result["mwindow"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            
            var id = int.Parse(jsonMaintenanceWindow["id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(maintenanceWindow.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            maintenanceWindow.Id = id.ToString();

            // JSON Validation
            Console.WriteLine();
            Console.WriteLine($"JSON Validation - GetMaintenanceWindows");

            result = _testbedService.GetMaintenanceWindows(apiUrl, apiKey, ResponseTypes.Json);                
            var jsonMaintenanceWindows = (List<object>)result["mwindows"];
            var jsonPagination = (IDictionary<string, object>)result["pagination"];

            Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
            ProcessRunMaintenanceWindowEditsGetMaintenanceWindowsAssertions(
                jsonPagination, 
                jsonMaintenanceWindows, 
                ResponseTypes.Json,
                maintenanceWindow,
                isDeleted);
        }

        private void ValidateMaintenanceWindowXml(
            string apiUrl, 
            string apiKey,
            IDictionary<string, object> result, 
            MaintenanceWindow maintenanceWindow, 
            bool isEdit = false, 
            bool isDeleted = false)
        {
            var xmlMaintenanceWindow = (IDictionary<string, object>)((IDictionary<string, object>)result["maintenance-window"]);
                            
            var id = int.Parse(xmlMaintenanceWindow["@id"].ToString());

            if (isEdit || isDeleted)
            {
                Assert.That(id.ToString, Is.EqualTo(maintenanceWindow.Id));     
            }
            else
            {
                Assert.That(id, Is.GreaterThanOrEqualTo(1));     
            }

            maintenanceWindow.Id = id.ToString();              

            // XML Validation
            Console.WriteLine();
            Console.WriteLine($"XML Validation - GetMaintenanceWindows");

            result = _testbedService.GetMaintenanceWindows(apiUrl, apiKey, ResponseTypes.Xml);                
            var xmlMaintenanceWindowsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["mwindows"]);
            var xmlPagination = (IDictionary<string, object>)xmlMaintenanceWindowsOuter["pagination"];
            var xmlMaintenanceWindows = (List<object>)xmlMaintenanceWindowsOuter["mwindow"];

            ProcessRunMaintenanceWindowEditsGetMaintenanceWindowsAssertions(
                xmlPagination,
                xmlMaintenanceWindows, 
                ResponseTypes.Xml,
                maintenanceWindow,
                isDeleted);
        }

        private void ProcessRunMaintenanceWindowEditsGetMaintenanceWindowsAssertions(
            IDictionary<string, object> pagination,
            List<object> maintenanceWindows,
            string format,
            MaintenanceWindow maintenanceWindowObj,
            bool isDeleted = false)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(0));

            var foundMaintenanceWindow = false;
            foreach (var maintenanceWindow in maintenanceWindows.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(maintenanceWindow[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
                
                if (maintenanceWindowObj.Id == maintenanceWindow[prependChar + "id"].ToString())
                {
                    foundMaintenanceWindow = true;
                    Assert.That(maintenanceWindow[prependChar + "friendly_name"].ToString(), Is.EqualTo(maintenanceWindowObj.FriendlyName));
                    Assert.That(maintenanceWindow[prependChar + "type"].ToString(), Is.EqualTo(maintenanceWindowObj.Type));
                    Assert.That(maintenanceWindow[prependChar + "value"].ToString(), Is.EqualTo(maintenanceWindowObj.Value));
                    break;
                }
            }

            if (!isDeleted)
            {
                Assert.IsTrue(foundMaintenanceWindow);
            }
            else
            {
                Assert.IsFalse(foundMaintenanceWindow);
            }
        }

        [Test]
        public void GetMaintenanceWindows()
        {
            var settings = _testbedService.GetSettings();
            for (var i = 0; i < settings.ApiUrls.Count; i++)
            {
                var apiUrl = settings.ApiUrls[i];
                var apiKey = settings.ApiKeys[i];

                Console.WriteLine();
                Console.WriteLine($"API URL: {apiUrl}");
                Console.WriteLine("-----------------------------------------");
            
                // JSON
                var result = _testbedService.GetMaintenanceWindows(apiUrl, apiKey, ResponseTypes.Json);                
                var jsonMaintenanceWindows = (List<object>)result["mwindows"];
                var jsonPagination = (IDictionary<string, object>)result["pagination"];

                Assert.That(result["stat"].ToString(), Is.EqualTo("ok"));
                ProcessGetMaintenanceWindowsAssertions(jsonPagination, jsonMaintenanceWindows, ResponseTypes.Json);

                // XML
                result = _testbedService.GetMaintenanceWindows(apiUrl, apiKey, ResponseTypes.Xml);                            
                var xmlMaintenanceWindowsOuter = (IDictionary<string, object>)((IDictionary<string, object>)result["mwindows"]);
                var xmlPagination = (IDictionary<string, object>)xmlMaintenanceWindowsOuter["pagination"];
                var xmlMaintenanceWindows = (List<object>)xmlMaintenanceWindowsOuter["mwindow"];
            
                ProcessGetMaintenanceWindowsAssertions(xmlPagination, xmlMaintenanceWindows, ResponseTypes.Xml);
            }
        }

        private void ProcessGetMaintenanceWindowsAssertions(
            IDictionary<string, object> pagination,
            List<object> maintenanceWindows,
            string format)
        {
            var prependChar = string.Empty;
            if (format == ResponseTypes.Xml)
            {
                prependChar = "@";
            }

            Assert.That(int.Parse(pagination[prependChar + "offset"].ToString()), Is.GreaterThanOrEqualTo(0));
            Assert.That(int.Parse(pagination[prependChar + "limit"].ToString()), Is.LessThanOrEqualTo(50));
            Assert.That(int.Parse(pagination[prependChar + "total"].ToString()), Is.GreaterThanOrEqualTo(1));

            foreach (var maintenanceWindow in maintenanceWindows.Select(x => (IDictionary<string, object>)x))
            {
                Assert.That(maintenanceWindow[prependChar + "friendly_name"].ToString(), Is.Not.Null.Or.Empty);
            }
        }
    }
}