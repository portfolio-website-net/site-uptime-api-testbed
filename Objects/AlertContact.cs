namespace SiteUptimeApiTestbed.Objects
{
    public class AlertContact
    {
        public string Id { get; set; }

        public string FriendlyName { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public string Value { get; set; }
    }
}