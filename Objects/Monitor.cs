namespace SiteUptimeApiTestbed.Objects
{
    public class Monitor
    {
        public string Id { get; set; }

        public string FriendlyName { get; set; }

        public string Url { get; set; }

        public string Type { get; set; }

        public string Port { get; set; }

        public string KeywordType { get; set; }

        public string KeywordValue { get; set; }

        public string Interval { get; set; }

        public string AlertContacts { get; set; }
    }
}