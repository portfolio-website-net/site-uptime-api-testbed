namespace SiteUptimeApiTestbed.Objects
{
    public class PublicStatusPage
    {
        public string Id { get; set; }

        public string FriendlyName { get; set; }

        public string Monitors { get; set; }

        public string CustomDomain { get; set; }

        public string Password { get; set; }

        public string Sort { get; set; }

        public string HideUrlLinks { get; set; }

        public string Status { get; set; }
    }
}