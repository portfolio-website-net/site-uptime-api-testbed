namespace SiteUptimeApiTestbed.Objects
{
    public class MaintenanceWindow
    {
        public string Id { get; set; }

        public string FriendlyName { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }

        public string StartTime { get; set; }

        public string Duration { get; set; }

        public string Status { get; set; }
    }
}