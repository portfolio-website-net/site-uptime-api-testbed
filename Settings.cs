﻿using System.Collections.Generic;

namespace SiteUptimeApiTestbed
{
    public class Settings
    {
        public List<string> ApiUrls { get; set; }

        public List<string> ApiKeys { get; set; }
    }
}
