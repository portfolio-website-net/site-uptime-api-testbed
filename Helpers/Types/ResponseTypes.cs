namespace SiteUptimeApiTestbed.Helpers.Types
{
    public class ResponseTypes
    {
        public const string Json = "json";
        public const string Xml = "xml";
    }
}
